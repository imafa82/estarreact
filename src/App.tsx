import React, { useEffect, useState} from 'react';
import {BrowserRouter as Router} from "react-router-dom";

import AuthRoute from "./core/routes/AuthRoute";
import PublicRoute from "./core/routes/PublicRoute";
import {http} from "./shared/utils/http";
import Auth from "./core/Auth";
import {UserModel} from "./models/User.model";
import {useAppDispatch, useAppSelector} from "./app/hooks";
import {
    autoLoginAction,
    logoutAction,
    resetUser,
    selectAuth,
    selectUser,
    setAuth,
    setUser
} from "./core/auth/authSlice";

function App(){
    // const [auth, setAuth] = useState<boolean>()
    // const [user, setUser] = useState<UserModel>();
    const auth = useAppSelector(selectAuth);
    const user = useAppSelector(selectUser);
    const dispatch = useAppDispatch();
    const logoutHandler = () => {
        dispatch(logoutAction())
    }
    useEffect(() => {
        localStorage.getItem('token') ? dispatch(autoLoginAction()) : dispatch(setAuth(false))
    }, [])

    return (
        <>
            <div className="container">
                <Router>
                    {
                        auth === undefined? <div>Caricamento in corso</div> :
                        <>
                            {auth ?
                                <AuthRoute user={user} logoutHandler={logoutHandler} /> :
                                <PublicRoute />}
                        </>
                    }

                </Router>
            </div>
        </>
    );


}

export default App;
