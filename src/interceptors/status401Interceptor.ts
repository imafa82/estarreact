import {store} from "../app/store";
import {logoutAction} from "../core/auth/authSlice";


const status401Interceptor = (err: any) => {
    if(err?.response?.status === 401){
        console.log('processo di logout')
        store.dispatch(logoutAction())
    }
    return Promise.reject(err);
}

export default status401Interceptor;
