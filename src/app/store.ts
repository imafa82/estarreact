import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import authReducer from "../core/auth/authSlice";
import actorsReducer from "../features/actors/actorsSlice";
import showReducer from "../features/show/showSlice";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    auth: authReducer,
    actors: actorsReducer,
    show: showReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
