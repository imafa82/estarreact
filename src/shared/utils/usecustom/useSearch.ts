import React, {useState} from "react";

export function useSearch(data: any[], properties: string[] = []){
    const [search, setSearch] = useState<string>('')

    const filterData = data.filter(ele =>
        (properties.length? properties : Object.keys(ele)).reduce((acc, property) => {
            return ele[property].toString().toLowerCase().trim().includes(search.toLowerCase().trim()) || acc
        }, false)

    );

    const searchHandler: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        setSearch(event.target.value);
    }

    return {
        search,
        setSearch,
        searchHandler,
        filterData
    }
}
