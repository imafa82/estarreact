export type ValueOf<T = any> = T[keyof T]
