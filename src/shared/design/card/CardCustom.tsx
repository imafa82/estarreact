import React from "react";
import {bootstrapType} from "../models/Bootstrap.model";

interface CardCustomProp{
    title?: string;
    children?: React.ReactNode
    classList?: string[]
    headerClass?: bootstrapType[]
    bodyClass?: bootstrapType[]
}
// headerClass = ['bg-primary']
const CardCustom: React.FC<CardCustomProp> = ({
                      title = 'Inserire titolo',
                      children,
                        classList = [],
                        headerClass= [],
                        bodyClass = []
}) => {
    const listClass = ['card', ...classList]
    const listHeader = ['card-header', ...headerClass]
    const listBody = ['card-body', ...bodyClass]
    return (
        <div className={listClass.join(' ')}>
            <div className={listHeader.join(' ')}>
                {title}
            </div>
            <div className={listBody.join(' ')}>
                {children}
            </div>
        </div>
    )
}

export default CardCustom;
