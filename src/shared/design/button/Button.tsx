import React from "react";
interface ButtonPropsModel {
    type?: 'button' | 'submit' | 'reset';
    classBt?: string;
    text?: string;
    children?: React.ReactNode;
    clickAction: () => void
}

function Button(props: ButtonPropsModel){
    let type: 'button' | 'submit' | 'reset'  = props.type || 'button';
    let classList = ['shared_button', 'btn', props.classBt || 'btn-primary']
    let text = props.text || 'Salva';

    return (
        <button onClick={props.clickAction} type={type} className={classList.join(' ')}>
            {props.children || text}
        </button>
    )
}
export default Button;
