import {ValueOf} from "../../utils/typescript/ValueOf";

export const classListTableData = {
    alternatingRow: 'alternating-row',
    test: 'test'
} as const;
// export type classListTableDataType = typeof classListTableData
// {
//     readonly alternatingRow: 'alternating-row';
//     readonly test: 'test';
// }

// export type classListTableDataKeys = keyof classListTableDataType;
// export type classListTableDataKeys = 'alternatingRow' | 'test';

export type classListTable = ValueOf<typeof classListTableData>
// export type classListTable = classListTableDataType[classListTableDataKeys]
//  classListTableDataType[keyof classListTableDataType]
// export type classListTable = classListTableDataType['alternatingRow' | 'test']
// export type classListTable = (classListTableDataType['alternatingRow'] | classListTableDataType['test'] )
// export type classListTable = 'alternating-row' | 'test'
