import React from "react";
import {classListTable} from "./TableModel";
interface TableCustomPropsModel{
    data?: any[];
    columns: {label: string, field: string}[]
    templates?: {[key: string]: (value: any, element: any) => React.ReactNode}
    primaryKey?: string
    classList?: classListTable[]
}
const TableCustom: React.FC<TableCustomPropsModel> = ({
    primaryKey, classList = [], data = [], columns, templates
  }) => {
    const classData = ['table-custom', ...classList]
    return (
        <table className={classData.join(' ')}>
            <thead>
            <tr>
                {columns.map(col => <th key={col.field}>{col.label}</th>)}
            </tr>
            </thead>
            <tbody>
            {
                data.map((element, index) => (
                    <tr key={primaryKey? element[primaryKey] : index}>
                        {columns.map(col => <td key={col.field}>
                            {templates && templates[col.field] ?
                                templates[col.field](element[col.field], element)
                                : element[col.field]}
                        </td>)}
                    </tr>
                ))
            }
            </tbody>
        </table>
    )
}

export default TableCustom;
