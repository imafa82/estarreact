import {http} from "../../shared/utils/http";
import {UserApi} from "../users/models/UserApi.model";

export const getUsers = () => http.get<UserApi[]>('https://jsonplaceholder.typicode.com/users')
