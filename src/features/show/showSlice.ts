import {createSelector, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {UserApi} from "../users/models/UserApi.model";
import {RootState} from "../../app/store";



export interface ShowState {
  users: UserApi[],
  index: number
}

const initialState: ShowState = {
   users: [],
   index: 0
};


export const showSlice = createSlice({
  name: 'show',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setUsers: (state, action: PayloadAction<UserApi[]>) => {
      state.users = action.payload;
    },
      resetUsers: (state) => {
          state.users = [];
      },
      setIndex: (state, action: PayloadAction<number>) => {
          state.index = action.payload;
      },
      resetIndex: (state) => {
          state.index = 0;
      },

  },
});

export const { setUsers, resetUsers, setIndex, resetIndex  } = showSlice.actions;



export const selectUsersApi = (state: RootState) => state.show.users;
export const selectIndexUserApi = (state: RootState) => state.show.index;
export const selectDisablePrev = (state: RootState) => state.show.index === 0;
export const selectDisableNext = createSelector(selectUsersApi, selectIndexUserApi, (users, index) => {
    return index === users.length -1
});


export default showSlice.reducer;
