import React, {useEffect, useState} from "react";
import {UserApi} from "../users/models/UserApi.model";
import {getUsers} from "./showAPI";
import UserInfo from "./components/UserInfo";
import {useAppDispatch, useAppSelector} from "../../app/hooks";
import {
    resetIndex,
    selectDisableNext,
    selectDisablePrev,
    selectIndexUserApi,
    selectUsersApi,
    setIndex,
    setUsers
} from "./showSlice";

const Show: React.FC = () => {
    // const [users, setUsers] = useState<UserApi[]>([])
    // const [index, setIndex] = useState<number>(0)
    const index = useAppSelector(selectIndexUserApi);
    const users = useAppSelector(selectUsersApi);
    const disablePrevButton = useAppSelector(selectDisablePrev);
    const disableNextButton = useAppSelector(selectDisableNext);
    const dispatch = useAppDispatch();
    useEffect(() => {
        getUsers().then(res => dispatch(setUsers(res)))
        return () => {
            dispatch(resetIndex())
        }
    }, [])
    const user = users[index]
    const prevUser = () => {
        dispatch(setIndex(index -1))
    }
    const nextUser = () => {
        dispatch(setIndex(index +1))
    }
    return <>
        {user && <UserInfo user={user} />}
        <div>
            <button onClick={prevUser} disabled={disablePrevButton}>Precedente</button>
            <button onClick={nextUser} disabled={disableNextButton}>Successivo</button>
        </div>
    </>

}

export default Show
