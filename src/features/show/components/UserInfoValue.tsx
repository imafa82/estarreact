import React from "react";
import {UserApi} from "../../users/models/UserApi.model";

interface UserInfoPropsModel{
    label: string;
    value: string
}

const UserInfoValue: React.FC<UserInfoPropsModel> = ({label, value}) => {

    return <>
        <label>{label}: </label>
        <span><strong> {value}</strong></span>
    </>

}

export default UserInfoValue
