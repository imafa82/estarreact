import React from "react";
import {UserApi} from "../../users/models/UserApi.model";
import UserInfoValue from "./UserInfoValue";

interface UserInfoPropsModel{
    user: UserApi
}

const UserInfo: React.FC<UserInfoPropsModel> = ({user}) => {
    const structure = [
        {
            label: 'Nome',
            field: 'name'
        },
        {
            label: 'Email',
            field: 'email'
        },
        {
            label: 'Username',
            field: 'username'
        },
    ]
    const structureAddress = [
        {
            label: 'Indirizzo',
            field: 'street',
            format: 'address'
        },
        {
            label: 'Città',
            field: 'city',
            format: 'address'
        }
    ]
    const sections: any = [
        {
            title: "Informazioni anagrafiche",
            list: structure
        },
        {
            title: "Indirizzo",
            list: structureAddress
        }
    ]
    const formatters: any = {
        address: (name: string) => {
            return (user as any).address[name]
        }
    }
    return <>
        {sections.map((sec: any) =>(
            <div>
                <h2>{sec.title}</h2>
                <div className="row">
                    {
                        sec.list.map((str: any) => <div className="col-6" key={str.field}>
                            <UserInfoValue
                                label={str.label}
                                value={str.format ? formatters[str.format](str.field) :  (user as any)[str.field]} />
                        </div>)
                    }
                </div>


            </div>
        ))}
    </>

}

export default UserInfo
