import {useEffect, useState} from "react";
import {UserApi} from "./models/UserApi.model";

export function useUsers(){
    const [users, setUsers] = useState<UserApi[]>([])
    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(res => setUsers(res))
    }, [])
    const removeUser = (id: number) => {
        fetch('https://jsonplaceholder.typicode.com/users/'+id, {method: 'DELETE'})
            .then(res => res.json())
            .then(res => {
                setUsers(users.filter(user => user.id !== id))
                console.log('remove user con id ' +id )
            })

    }
    return {
        users,
        setUsers,
        removeUser
    }
}
