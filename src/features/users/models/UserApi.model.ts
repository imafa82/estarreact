export interface UserApi {
    id: number
    name: string;
    username: string;
    email: string
    address: UserApiAddress,
    addressStreet?: string
}


export interface UserApiAddress {
    city: string;
    geo: any;
    street: string;
    suite: string;
    zipcode: string
}
