import React, {useState} from "react";
import {UserApi} from "./models/UserApi.model";
import UserDetail from "./components/UserDetail";
import UsersList from "./components/UsersList";

const Users: React.FC = () => {
    const [selectedUser, setSelectedUser] = useState<number>()
    const showUserDetail = (id: number) => {
        setSelectedUser(id);
    }
    return <>
            {selectedUser ?
                <UserDetail
                    idUser={selectedUser}
                    backClick={() => setSelectedUser(undefined)}
                /> :
                <UsersList showUserDetail={showUserDetail} />}
    </>

}

export default Users
