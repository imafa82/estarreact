import {UserApi} from "./models/UserApi.model";

export const usersColumns: {label: string, field: keyof UserApi | 'actions'}[] = [
    {
        label: 'Id',
        field: 'id'
    },
    {
        label: 'Nome',
        field: 'name'
    },
    {
        label: 'Username',
        field: 'username'
    },
    {
        label: 'Email',
        field: 'email'
    },
    {
        label: 'Indirizzo',
        field: 'address'
    },
    {
        label: '',
        field: 'actions'
    }
]
