import React, {useCallback, useState} from "react";
import TableCustom from "../../../shared/design/table/TableCustom";
import {classListTableData} from "../../../shared/design/table/TableModel";
import {useUsers} from "../useUsers";
import {useTableUsers} from "../useTableUsers";
import {useSearch} from "../../../shared/utils/usecustom/useSearch";
import {useNavigate} from "react-router-dom";
import TotUser from "./TotUser";

interface UsersListPropsModel{
    showUserDetail: (id: number) => void
}

const UsersList: React.FC<UsersListPropsModel> = ({showUserDetail}) => {
    const {users, removeUser} = useUsers();
    const navigate = useNavigate()
    const [display, setDisplay] = useState(false);
    const {columns, templates} = useTableUsers(removeUser, showUserDetail);
    const {search, searchHandler, filterData} = useSearch(users)
    const goActorsPage = () => {
        navigate('actors');
    }
    const displayHandler = () => {
        setDisplay(!display)
    }
    const testHandler = useCallback(() => {
        console.log(display)
    }, [display])
    return <>
        <button onClick={goActorsPage}>Pagina attori</button>
        <button onClick={displayHandler}>{display? 'Nascondi' : 'Mostra'}</button>
        {display && <input value={search} onChange={searchHandler} type="text"/>}
        <div>variabile da cercare: {search}</div>
        <TableCustom classList={[classListTableData.alternatingRow]}
                     primaryKey="id"
                     templates={templates}
                     columns={columns}
                     data={filterData} />
        <TotUser tot={filterData.length}  test={testHandler} />
    </>
}

export default UsersList;
