import React, {useEffect, useState} from "react";
import {UserApi} from "../models/UserApi.model";
interface UserDetailPropsModel{
    idUser: number
    backClick: () => void
}
const UserDetail: React.FC<UserDetailPropsModel> = ({idUser, backClick}) => {
    const [selectedUser, setSelectedUser] = useState<UserApi>()
    const callUser = () => {
        fetch(`https://jsonplaceholder.typicode.com/users/${idUser}`)
            .then(res =>res.json())
            .then(res => {
                setSelectedUser(res);
             })
    }
    useEffect(() => {
        callUser();
    }, [])
    return (
        <div>
            <button onClick={backClick}> Torna alla pagina degli utenti</button>
            {selectedUser ? JSON.stringify(selectedUser) : <div>Carimento in corso</div>}
        </div>
    )
}

export default UserDetail;
