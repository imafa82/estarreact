import React, {useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../../app/hooks";
import {selectDisplay, setDisplay} from "../../../core/auth/authSlice";

interface TotUserPropsModel{
    tot: number;
    test: () => void
}

const TotUser: React.FC<TotUserPropsModel> = ({tot = 0, test}) => {
    // const [display, setDisplay] = useState(false)
    const display = useAppSelector(selectDisplay)
    const dispatch = useAppDispatch();
    useEffect(() => {
        console.log('passo di qui')
    })
    useEffect(() => {
        test && test();
    }, [])
    return (
        <>
            {display && <div>{tot}</div>}
            <button onClick={() => dispatch(setDisplay(!display))}>Mostra/Nascondi</button>
            <button onClick={test}>Stampa test </button>
        </>
    )
}

export default React.memo(TotUser);
