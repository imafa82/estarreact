import {usersColumns} from "./usersData";
import {UserApi, UserApiAddress} from "./models/UserApi.model";
import React from "react";

export function useTableUsers(removeUser: (id: number) => void, showUserDetail: (id: number) => void){
    const templates = {
        name: (value: string) => {
            return <div>{value.toUpperCase()}</div>
        },
        address: (value: UserApiAddress) => {
            return <div>{value.street}, {value.suite}</div>
        },
        actions: (value: any, row: UserApi) => {
            return <>
                <i className="fa fa-user" onClick={() => showUserDetail(row.id)}></i>
                <i className="fa fa-trash" onClick={() => removeUser(row.id)}></i>
            </>
        }
    }

    return {
        columns: usersColumns,
        templates
    }
}
