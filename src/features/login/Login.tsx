import React, {ChangeEvent, useState} from "react";
import {UserModel} from "../../models/User.model";
import {http} from "../../shared/utils/http";
import {useAppDispatch} from "../../app/hooks";
import {setAuth, setUser} from "../../core/auth/authSlice";
import { Formik } from 'formik';
interface LoginRequestModel{
    email: string;
    password: string;
}
const Login: React.FC = () => {
    const dispatch = useAppDispatch();
    const loginHandler = (values: LoginRequestModel, setSubmitting: (data: boolean) => void) => {
        http.post<{token: string, user: UserModel}>(`login`, values, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res =>{
            localStorage.setItem('token', res.token)
            dispatch(setAuth(true))
            dispatch(setUser(res.user))
            setSubmitting(false);
        }, err => {
            console.log(err)
            setSubmitting(false);
        })
    }

    return <div>
        <h1>Login</h1>
        <Formik
            initialValues={{ email: '', password: '' }}
            validate={values => {
                const errors: any = {};
                if (!values.email) {
                    errors.email = 'Required';
                } else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                    errors.email = 'Invalid email address';
                }
                return errors;
            }}
            onSubmit={(values, { setSubmitting }) => {

                    loginHandler(values, setSubmitting)

            }}
        >
            {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  /* and other goodies */
              }) => (
                <form onSubmit={handleSubmit}>
                    <input
                        type="email"
                        name="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                    />
                    {errors.email && touched.email && errors.email}
                    <input
                        type="password"
                        name="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                    />
                    {errors.password && touched.password && errors.password}
                    <button type="submit" disabled={isSubmitting}>
                        Login
                    </button>
                </form>
            )}
        </Formik>
    </div>
}

export default Login;
