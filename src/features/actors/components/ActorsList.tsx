import React from "react";
import {ActorModel} from "../models/Actor.model";
import * as Yup from "yup";
import {useFormik} from "formik";
import {useAppDispatch} from "../../../app/hooks";
import {addActor} from "../actorsSlice";

interface ActorListPropsModel{
    actors: ActorModel[];
    clickActor: (actor: ActorModel) => void
}

const ActorsList: React.FC<ActorListPropsModel> = ({actors, clickActor}) => {
    const dispatch = useAppDispatch();
    const validations = Yup.object().shape({
        name: Yup.string().required("il nome è obbligatorio"),
        surname: Yup.string().required("il cognome è obbligatorio"),
        year: Yup.number().required("l'anno è obbligatorio"),
    })
    const submitForm = (values: any) => {
        setTimeout(() => {
            dispatch(addActor({_id: 'test23', ...values, movies: []}))
        }, 400)
    }
    const formik = useFormik({
        initialValues: {},
        validationSchema: validations,
        onSubmit: (values) => {
            submitForm(values)
        }
    })
    const structure = [
        {
            label: 'Nome',
            name: 'name',
            typeElement: 'input'
        },
        {
            label: 'cognome',
            name: 'surname',
            typeElement: 'input'
        },
        {
            label: 'Anno di nascita',
            name: 'year',
            typeElement: 'select',
            options: [
                {
                    value: 2010,
                    label: '2010'
                }
            ]
        }
    ]
    return <>
        <h1>Lista attori</h1>
        <div>
            {
                actors.map(actor => <div onClick={() => clickActor(actor)} key={actor._id}>{actor.name} {actor.surname}</div>)
            }
        </div>
        <div>
            <h2>Aggiunta attore</h2>
            <form onSubmit={formik.handleSubmit}>
                {structure.map(str => <div className="form-group">
                    <label>{str.label}</label>
                    <input
                        value={(formik.values as any)[str.name]}
                        name={str.name}
                        onChange={(event) => formik.setFieldValue(str.name, event.target.value)}
                    />
                    {(formik.errors as any)[str.name] && <div>{(formik.errors as any)[str.name]}</div>}
                </div>)}
                {/*<input value={(formik.values as any).name} name="name"*/}
                {/*       onChange={(event) => formik.setFieldValue('name', event.target.value)}/>*/}
                {/*<input value={(formik.values as any).surname} name="surname"*/}
                {/*       onChange={(event) => formik.setFieldValue('surname', event.target.value)}/>*/}
                {/*<input value={(formik.values as any).year} name="year"*/}
                {/*       onChange={(event) => formik.setFieldValue('year', event.target.value)}/>*/}

                {JSON.stringify(formik.errors)}
                <button>Salva</button>
            </form>
        </div>
    </>
}

export default ActorsList;
