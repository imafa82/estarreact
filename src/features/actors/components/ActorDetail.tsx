import React from "react";
import {ActorModel} from "../models/Actor.model";
interface ActorDetailPropsModel{
    actor: ActorModel,
    resetActor: () => void
}
const ActorDetail: React.FC<ActorDetailPropsModel> = ({actor, resetActor}) => {
    return <div>
        <h1>Dettaglio {actor.name} {actor.surname}</h1>
        {actor.movies.length ? <div>
                <h2>Lista film</h2>
                <div>
                    <ul>
                        {actor.movies.map(movie => <li key={movie._id}>{movie.title}</li>)}
                    </ul>
                </div>
            </div> :
            <div>Nessun film per l'attore selezionato</div>}
        <button onClick={resetActor}>Indietro</button>
    </div>
}

export default ActorDetail;
