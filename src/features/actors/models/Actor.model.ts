export interface ActorModel{
    name: string;
    surname: string;
    year: number;
    _id: string;
    movies: MovieModel[]
}

export interface MovieModel{
    _id: string;
    title: string;
    year: number;
}
