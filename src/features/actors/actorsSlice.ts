import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../../app/store';
import {UserModel} from "../../models/User.model";
import {incrementByAmount, selectCount} from "../../features/counter/counterSlice";
import {http} from "../../shared/utils/http";
import {ActorModel} from "./models/Actor.model";
import {setAuth, setUser} from "../../core/auth/authSlice";
import {getActors} from "./ActorsAPI";
import {
    getLocalStorageData,
    issetLocalStorageData,
    setLocalStorageData, setStoreByLocalStorage
} from "../../core/localstorage/localStorageService";



export interface ActorsState {
  actors: ActorModel[];
  actor?: ActorModel;
  lastUpdate?: string;
}

const initialState: ActorsState = {
    actors: []
};


export const actorsSlice = createSlice({
  name: 'actors',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setActors: (state, action: PayloadAction<ActorModel[]>) => {
      state.actors = action.payload;
    },
    setActor: (state, action: PayloadAction<ActorModel>) => {
        state.actor = action.payload;
    },
      addActor: (state, action: PayloadAction<ActorModel>) => {
          state.actors = [...state.actors, action.payload];
      },
      removeActor: (state, action: PayloadAction<string>) => {
          state.actors = state.actors.filter(act => act._id !== action.payload);
      },
      setLastUpdate: (state, action: PayloadAction<string>) => {
          state.lastUpdate = action.payload;
      },
      resetActor: (state) => {
          state.actor = undefined;
      }
  },
});

export const { setActors, setActor, resetActor, setLastUpdate, addActor, removeActor } = actorsSlice.actions;

export const getActorsAction =
    (): AppThunk =>
        (dispatch, getState) => {
            const actors = getState().actors.actors;
            if(!actors.length && issetLocalStorageData('actors')){
                const callback = (date: string) => {
                    console.log(date)
                    dispatch(setActors(getLocalStorageData('actors')));
                    dispatch(setLastUpdate(date))
                }
                setStoreByLocalStorage(callback)
            }else if(!actors.length){
                getActors().then(res => {
                    dispatch(setActors(res));
                    setLocalStorageData('actors', res)
                });
            }
        };

export const selectActors = (state: RootState) => state.actors.actors;
export const selectActor = (state: RootState) => state.actors.actor;


export default actorsSlice.reducer;
