import {http} from "../../shared/utils/http";
import {ActorModel} from "./models/Actor.model";

export const getActors = () => http.get<ActorModel[]>('actors')
