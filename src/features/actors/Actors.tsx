import React, {useEffect, useState} from "react";
import {getActors} from "./ActorsAPI";
import {ActorModel} from "./models/Actor.model";
import ActorsList from "./components/ActorsList";
import ActorDetail from "./components/ActorDetail";
import {useAppDispatch, useAppSelector} from "../../app/hooks";
import {getActorsAction, resetActor, selectActor, selectActors, setActor, setActors} from "./actorsSlice";

const Actors: React.FC = () => {
    // const [actors, setActors] = useState<ActorModel[]>([]);
    // const [actor, setActor] = useState<ActorModel>();
    const actors = useAppSelector(selectActors);
    const actor = useAppSelector(selectActor);
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(getActorsAction())
        return () => {
            dispatch(resetActor())
        }
    }, [])

    const clickActor = (actor: ActorModel) => {
        dispatch(setActor(actor))
    }
    return (
        <div>
            {
                actor?
                    <ActorDetail actor={actor} resetActor={() => dispatch(resetActor())} /> :
                    <ActorsList actors={actors} clickActor={clickActor} />
            }
        </div>
    )
}

export default Actors;
