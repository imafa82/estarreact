import React from "react";
import { NavLink } from "react-router-dom";
import auth from "../routes/auth";

const MenuLeft: React.FC = () => {
    const routes = auth.list.filter(route =>route.navTitle);
    return (
        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {routes.map(route => (
                <li key={route.path} className="nav-item">
                    <NavLink to={route.path}
                             className={({isActive}) => isActive ? 'nav-link text-info' : 'nav-link'}
                    >{route.navTitle}</NavLink>
                </li>
            ))}
        </ul>
    )
}

export default MenuLeft
