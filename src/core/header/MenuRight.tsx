import React from "react";
import { NavLink } from "react-router-dom";
import {UserModel} from "../../models/User.model";
import MenuLeft from "./MenuLeft";
interface HeaderPropsModel{
    logout: () => void
    user?: UserModel
}
const MenuRight: React.FC<HeaderPropsModel> = ({logout, user}) => {
    return (
        <>
            {user && <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        {user.name} {user.surname}
                    </a>
                    <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="navbarScrollingDropdown">
                        <li onClick={logout}><a className="dropdown-item" href="#">Logout</a></li>
                    </ul>
                </li>
            </ul>}
        </>
    )
}

export default MenuRight
