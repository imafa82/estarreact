import React from "react";
import { NavLink } from "react-router-dom";
import {UserModel} from "../../models/User.model";
import MenuLeft from "./MenuLeft";
import MenuRight from "./MenuRight";
import HeaderLayout from "./HeaderLayout";
interface HeaderPropsModel{
    logout: () => void
    user?: UserModel
}
const Header: React.FC<HeaderPropsModel> = ({logout, user}) => {
    return (
        <HeaderLayout
            menuLeft={<MenuLeft />}
            menuRight={<MenuRight logout={logout} user={user}/>}
        />
    )
}

export default Header
