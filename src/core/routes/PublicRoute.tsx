import React from "react";
import { Route, Routes, Navigate} from "react-router-dom";
import publicRoutes from "./public";
import DynamicRoute from "./DynamicRoute";

const PublicRoute: React.FC = () => <DynamicRoute routesList={publicRoutes} />

export default PublicRoute;
