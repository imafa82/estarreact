import {RoutesModel} from "./models/Routes.model";
import Users from "../../features/users/Users";
import Actors from "../../features/actors/Actors";
import Show from "../../features/show/Show";

const auth: RoutesModel = {
    list: [
        {
            path: '/users',
            name: 'users.index',
            navTitle: 'Utenti',
            component: <Users />
        },
        {
            path: '/actors',
            name: 'actors.index',
            navTitle: 'Attori',
            component: <Actors />
        },
        {
            path: '/test',
            name: 'actors.index',
            component: <Actors />
        },
        {
            path: '/show',
            name: 'show.index',
            navTitle: 'Show',
            component: <Show />
        },
    ],
    redirect: 'users'
}

export default auth;
