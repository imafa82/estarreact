import React from "react";
import Header from "../header/Header";
import { Route, Routes, Navigate} from "react-router-dom";
import auth from "./auth";
import {UserModel} from "../../models/User.model";
import DynamicRoute from "./DynamicRoute";
interface AuthRoutePropsModel{
    logoutHandler: () => void
    user?: UserModel
}
const AuthRoute: React.FC<AuthRoutePropsModel> = ({logoutHandler, user}) => {
    return (
      <>
          <Header logout={logoutHandler} user={user} />
          <DynamicRoute
            routesList={auth}
          />
      </>
    );
}

export default AuthRoute;
