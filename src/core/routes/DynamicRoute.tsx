import React from "react";
import { Route, Routes, Navigate} from "react-router-dom";
import {RoutesModel} from "./models/Routes.model";

interface DynamicRoutePropsModel{
    routesList: RoutesModel
}
const DynamicRoute: React.FC<DynamicRoutePropsModel> = ({routesList}) => {
    return (
            <Routes>
                {routesList.list.map((route) => <Route
                    key={route.path}
                    path={route.path}
                    element={route.component}/>)}
                {routesList.redirect && <Route
                    path="*"
                    element={<Navigate to={routesList.redirect} replace />}
                />}
            </Routes>
    );
}

export default DynamicRoute;
