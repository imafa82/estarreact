import React from "react";

export interface RoutesModel{
    list: {
        path: string;
        component: React.ReactComponentElement<any>;
        name: string;
        navTitle?: string;
        permissions?: string[]
    }[],
    redirect?: string
}
