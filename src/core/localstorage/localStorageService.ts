export const getLocalStorageData = (name: string) =>  JSON.parse(localStorage.getItem(name) as string)
export const issetLocalStorageData = (name: string) =>  !!localStorage.getItem(name)
export const setLocalStorageData = (name: string, data: any) =>  localStorage.setItem(name, JSON.stringify(data))
export const setStoreByLocalStorage = (callback: (date: string) => void) => {
    setTimeout(() => {
        console.log('set timeout')
        callback(new Date().toLocaleString())
    }, 500)
}
