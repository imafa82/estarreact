import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../../app/store';
import {UserModel} from "../../models/User.model";
import {incrementByAmount, selectCount} from "../../features/counter/counterSlice";
import {http} from "../../shared/utils/http";



export interface AuthState {
  auth?: boolean;
  user?: UserModel;
  display: boolean;
}

const initialState: AuthState = {
    display: false
};


export const authSlice = createSlice({
  name: 'auth',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    resetUser: (state) => {
      state.user = undefined;
    },
    setUser: (state, action: PayloadAction<UserModel>) => {
      state.user = action.payload;
    },
    setAuth: (state, action: PayloadAction<boolean>) => {
      state.auth = action.payload;
    },
      setDisplay: (state, action: PayloadAction<boolean>) => {
          state.display = action.payload;
      },
  },
});
export const { setUser, resetUser, setAuth, setDisplay } = authSlice.actions;

export const autoLoginAction =
    (): AppThunk =>
        (dispatch) => {
          http.get('users/logged').then(res => {
            dispatch(setAuth(true))
            dispatch(setUser(res))
          })
        };

export const logoutAction =
    (): AppThunk =>
        (dispatch) => {
          localStorage.removeItem('token');
          dispatch(setAuth(false))
          dispatch(resetUser());
        };



export const selectUser = (state: RootState) => state.auth.user;
export const selectAuth = (state: RootState) => state.auth.auth;
export const selectDisplay = (state: RootState) => state.auth.display;


export default authSlice.reducer;
